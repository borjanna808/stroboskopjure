# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git init
git remote add origin "https://JureBevc@bitbucket.org/JureBevc/stroboskop.git"
git pull origin master
```

Naloga 6.2.3:
https://bitbucket.org/JureBevc/stroboskop/commits/c9e60059aaa2c96f2467cb2568f696dbb0c8af93

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/JureBevc/stroboskop/commits/20a0ee420d309833f51bc011f210d54566aebe8a

Naloga 6.3.2:
https://bitbucket.org/JureBevc/stroboskop/commits/4122188e38711ccd9a45ffd752e432f727f6f3c9

Naloga 6.3.3:
https://bitbucket.org/JureBevc/stroboskop/commits/91e51dc3e0366e6366ecb5deb02d130cd6d8d7fe

Naloga 6.3.4:
https://bitbucket.org/JureBevc/stroboskop/commits/d676627257ab1f9a2a51a7c593c938b09aa70b26

Naloga 6.3.5:

```
git checkout master
git merge izgled
git push origin master
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/JureBevc/stroboskop/commits/e5cef17a20cf65aa3dc2f425b56eeb4099f826ef

Naloga 6.4.2:
https://bitbucket.org/JureBevc/stroboskop/commits/d1cdcdd9f40aa9ec709d35b498d9492a9538aea9

Naloga 6.4.3:
https://bitbucket.org/JureBevc/stroboskop/commits/7d50b927b20830f20c0307055e3cec8050fcdeb4

Naloga 6.4.4:
https://bitbucket.org/JureBevc/stroboskop/commits/7e2416f7bbd22ec2e8693f462583988f8a464b92